# Simple merges

## Instructions

**Before proceeding be sure that you have created your own branch for your changes by using the following command (make up a different name than what is given):**
```sh
git checkout -b <my-new-branch-name>
```

In the next section find the subsection with your name on it.
Fill out the short questionnaire and save the file.
Stage and commit your changes then push back to origin.

## Participants

### Tutor Paul

* Middle initial: W
* Dietary restrictions: Vegetarian
* Preferred Pastime: Play Rocket League with Sammy
* Pick a number: 2112

### momo3393

* Middle initial: M
* Dietary restrictions: No Dairy
* Preferred Pastime: napping
* Pick a number: 123
