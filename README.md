# Hi

Hello everybody! This is a repository devoted to experimenting with `git` in order to better understand how to interact with repositories, commits, merges, and possibly rebases.

As we go through our exercises I will ask that you interact with one specific files at a minimum, please see [SIMPLE.md] and follow the instructions in the file after creating a branch for yourself. After you have followed those instructions go ahead and create some files and folders for yourself. Create as many commits as you want and push your changes back to origin.


## Commands of interest

* Create and checkout new branch

```sh
git checkout -b <new-branch-name>
```

* See changes in working tree

```sh
git diff
```

* Check status of working tree

```sh
git status
```

* Stage files for commit

```sh
git add <file-to-stage>
```

* Commit all staged files with message

```sh
git commit -m "My special commit"
```

* Push commits to origin

```sh
git push
```

* Rebase last 4 commits interactively

```sh
git rebase --interactive HEAD~4
```

* See tree of all commits

```sh
git log --all --graph --color
```

* Fetch any changes made in origin

```sh
git fetch origin
```

* Apply commits in *other-branch* to the current branch

```sh
git merge other-branch
```

* Store changes to working tree apart from a commit

```sh
git stash
```

* Retrieve stashed changes

```sh
git stash pop
```
